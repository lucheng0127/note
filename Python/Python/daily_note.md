### @property
@property 可以将python定义的函数“当做”属性访问例如：
```
class User():
    nickname = models.CharField(max_length=30)
    is_admin = models.BooleanField(default=False)

    @property
    def is_staff(self):
        return self.is_admin
```
User.is_staff 就可以调用is_staff函数

### super()用法：
