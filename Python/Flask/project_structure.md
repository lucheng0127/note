### pycharm professional 激活地址：   
[http://idea.pjoc.pub](http://idea.pjoc.pub)
[http://idea.imsxm.com/](http://idea.imsxm.com/)
[http://114.215.133.70:41017﻿/](http://114.215.133.70:41017﻿/)
[http://172.245.22.235:1017/](http://172.245.22.235:1017/)
[http://mcpmcc.com:1017 ](http://mcpmcc.com:1017 )
### 使用虚拟环境：(推荐使用pyenv 和pyenv virtualenv)
* 新建一个名为venv的虚拟环境
> virtualenv venv
* 激活虚拟环境                  
> source venv/bin/activate
* 返回全局解释器
> deactivate

[pyenv安装及使用](https://github.com/yyuu/pyenv)
### Flask包导入顺序：
1. 系统库(os, datetime ...)
2. flask 拓展(flask_login ...)
3. 上层目录文件(from .. import *)
4. 当前目录文件(from . import *)

例如:
```
import os
form flask_login import login_required
from .. import utils
from . import models
```
### 程序开头标明编码格式：
```
# -*- coding: utf-8 -*-
```
### 固定可以引用的函数
```
_all_ = ['*', '*']
```
### 大型程序基本结构：
```
wolfsly
	application/		#主程序包
		blueprint1/
			__init__.py
			form.py
			views_1.py
			views_2.py
		blueprint2/
		blueprint3/
		static/		#静态文件
		templates/	#模板
			blueprint1/
			blueprint2/
		files/	#文件存储文件夹
		__init__.py		#申明app
		app.py		#create_app()
		configuration.py	#配置文件
		constants.py	#程序所需常量
		extensions.py	#程序所需拓展例如（flask_login, peewee...）
		models.py		#数据库模型
		utilities.py	#自己创建的函数
	documents/	#测试数据
	logs/	#运行logs
	tools/	#数据库脚本
	webservers/	#部署脚本
	wsgi.py		#部署程序入口
	wsgi-dev.py		#开发程序入口
	requirement.txt		#依赖列表
```
### 配置选项：（生产环境中debug安全起见应该设置为False）
程序的配置放于config.py
在config.py中最好定义多个配置例如开发，测试和生产环境的不同配置。
不同的配置用不同的类（class）定义。

* 程序包：
 程序包用来保存程序的所有代码及稳态文件，可以直接称为app 或者专门取程序名例如上述结构中的main。

* 方法一：使用程序工程函数：(放置于__init__.py)
create_app()，只接受一个参数，即程序使用的配置名。配置类在config.py中定义，导入可通过app.config.from_object(config[config_name])导入。
配置完再初始化   init_app()

* 方法二：使用蓝版：
就是在程序包中定义许多子包例如app/login和app/main或者app/dashboard等就是把大程序划分为几个小程序组成，每个小程序负责一部分功能。
 * 1.创建蓝版：
  ```
  #app/main/__init__.py
  from flask import Blueprint
  main = Blueprint('main',__name__)
  from . import views,errors
  ```


> (蓝版的构造函数需要两个参数，蓝版名和蓝版所在的包或者模板ps:一般第二个参数用__name__即可）
程序的路由函数在app/main/views.py模板中，错误处理在app/main/error.py。在app/main/__init__.py脚本的末尾导入路由模板和错误处理模板。

### 启动脚本：    
```
manage.py：
    import os
    from app import create_app, db
    from app.models import User, Role
    from flask_script import Manager, Shell
    from flask_migrate import Migrate, MigrateCommand

    app = create_app(os.getenv('Flask_CONFIG') or 'default')
    manager = Manager(app)
    migrate = Migrate(app,db)

    def make)shell_context():
        return dict(app=app, db=db, User=User, Role=Role)
    manager.add_command("shell", Shell(make_context_shell_contex))
    manager.add_command('db', MigrateCommand)

    if __name__ == '__main__':
        manager.run()
```        
### 需求文件： requirements.txt
* 导入：
```
(venv) $ pip2.7 freeze >requirements.txt
```
* 安装:
```
pip install -r requirement.txt
```


### 测试：

### 创建数据库：
```
python2.7 manage.py db upgrade
```  

### 使用WTForms注意事项：
1. 使用CSRF
2. 验证用户名或者email是否已经存在：

在forms.py定义的class末尾添加class的内置函数
```
def validate_email(self, field):
    if User.query.filter_by(email=field.data).first():
        raise ValidationError('email is already registered.')

def validata_username(self, field):
    if User.query.filter_by(username=field.data).first():
        raise ValidationError('username is already registered.')
```

### 数据库
* 关系型：
[参见peewee](https://github.com/coleifer/peewee)
