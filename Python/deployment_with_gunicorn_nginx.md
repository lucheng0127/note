[详情参见](https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-debian-8)
1. 修改/etc/nginx/nginx.conf:
在http中添加
```
include /usr/local/etc/nginx/sites-enabled/*;
```
2. 在/etc/nginx/sites-avalible中新建一个配置，再ln -s 改文件到/etc/nginx/sites-enabled中

3. gunicorn配置和nginx配置参见[gunicorn官方文档](http://docs.gunicorn.org/en/stable/deploy.html)

### nginx static file not found
solved：
```
server {   
     listen   8000;   
     server_name  localhost;

     access_log  /var/log/nginx/aa8000.access.log;    
     error_log  /var/log/nginx/aa8000.error.log;    

       location / {   
           index  index.html index.htm;    
       }    

     location ^/static/ {    
        autoindex on;    
        root   /opt/aa/webroot/;    
     }    
 }
```
### Django gunicorn nginx deploy
#### gunicorn static field not found:
> pip install dj-static

edit settings.py:

```
STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'
```
wsgi.py:
```
from django.core.wsgi import get_wsgi_application
from dj_static import Cling

application = Cling(get_wsgi_application())
```
url.py:
```
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns += staticfiles_urlpatterns()
```
