### Django前后分离的基本结构
* form用ModelForm通过form.save()来保存数据  
eg:  
```
form = FeedbackModForm(**mod_parmars, instance=feedback_obj)
if form.is_valid():
    form.save()
```
* models中可以用validators来对保存的数据进行数据可靠性验证例如XSS检验。  
eg：(validator_data 里面进行特殊字符校验)
```
from utils.validators import validator_data
class FeedBack(models.Model):
    id = models.AutoField(primary_key=True)
    subsystem = models.CharField(max_length=20, blank=False, null=False, validators[validator_data])  # 子系统
```
* serializers中定义json的数据结构，类似于原来用form进行数据传输的form
* 当前端将数据序列化后需要json.loads
