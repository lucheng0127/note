### 自定义User Model：
```
from.django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    pass
```
** before you migrate在setting中设置AUTH_USER_MODEL such as： **
```
from django.conf import settings

settings.AUTH_USER_MODEL = 'user.MyUser'
```

（AUTH_USER_MODEL = ’app名.model_name’）


### 和User model关联：
详见：Django官方文档Using Django中，search（referencing the user model）
example：
```
from django.conf import settings
from django.db import models

class Article(models.Model):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
```
