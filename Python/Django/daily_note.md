### Django gunicorn nginx deploy
#### gunicorn static field not found:
> pip install dj-static

edit settings.py:

```
STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'
```
wsgi.py:
```
from django.core.wsgi import get_wsgi_application
from dj_static import Cling

application = Cling(get_wsgi_application())
```
url.py:
```
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns += staticfiles_urlpatterns()
```

### Django DateTimeField or DateField and TimeField
这三个field都拥有相同的参数auto_now(), auto_now_add()
> auto_now=True   
（model被创建的时候的时间）

> auto_now_add=True
（model被修改时候的时间）

eg：
```
class Note(models.Model):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='note',
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=40)
    content = models.TextField(null=True)
    create_date = models.DateTimeField(auto_now_add=True) #model创建的时间
    update_date = models.DateTimeField(auto_now=True) #model每次修改的时间
```
> 注： 这两都是只读属性，不可以手动更改

若要可以手动更改可如下:
```
from django.db import models
import django.utils.timezone as timezone
class Doc(models.Model):
    add_date = models.DateTimeField('保存日期',default = timezone.now)
    mod_date = models.DateTimeField('最后修改日期', auto_now = True)
```


###
username:
> Admin


password:
> 1234qwer


### 自定义用户模型
[参见](http://www.cnblogs.com/daliangtou/p/5435385.html)

### 在setting.py的INSTALLED_APPS中添加app正确做法如下
```
INSTALLED_APPS = [
    'mysite.polls',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```
如果安装官方文档会显示no models name polls (错误做法如下:)
>  'mysite.polls.apps.PollsConfig',


### 数据库迁移做法如下:
```
python manage.py makemigrations
python manage.py migrate --run-syncdb
```
* 数据库查询时pk是primary key的意思

### 数据库基本操作语句:
表之间关系如下:
```
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    def __str__(self):
        return self.question_text

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text
```
操作如下:
```
>>> Question.objects.filter(id=1)  #可以获取多个object
>>> q = Question.objects.get(id=2)  #只可以获取一个object

>>> q.choice_set.all()
>>> q.choice_set.create(choice_text='Not much', votes=0)
>>> c = q.choice_set.create(choice_text='Just hacking again', votes=0)
>>> c.question.question_text
```


### url中name的作用:
在template里面可以直接用url函数生成对于name的url,例如:
> url(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'), #url中定义的路由
要生成路由可直接如下:


```
{% url 'polls:detail' question_id %}
```

### 路由中生成url 用reverse:(类似template里面的url)例如:
```
client.get(reverse('polls:index'))
```

### Python 行分割
```
lines = form.courseData.data.splitlines()
```

### django.shortcuts
* render
```
return render(request, 'note/note/note_list.html')
```
* redirect
```
from django.urls import reverse

home_url = reversse('HomePage')

return redirect(home_url)
```

### url 参数传递格式
> http://10.46.100.110:8000/list/proj_list/?page=2&proj_id=PVD
不同参数间用&连接

### Django 获取浏览器类型
> request.META['HTTP_USER_AGENT']
