依赖的模块：re  
re模块使python拥有全部正则表达式功能。  
* match  
尝试从字符串的起始位置匹配，若匹配返回，否则返回none
> re.match(pattern, string, flags=0)  
pattern 正则表达式的表达式  
string 要匹配的字符串  
flags 标志位，用于控制正则表达式的匹配方式，如：是否区分大小写，多行匹配等

re.match方法匹配成功将返回一个对象，否则返回None，将返回的对象赋值给matchObj后可以使用
matchObj.groups()返回一个匹配字符串的元组，从1到所含的小组号,获取单个匹配元素group(num)num
对应是匹配的第几个。

* search
扫描整个字符串并返回第一个成功的匹配。
> re.search(pattern, string, flags=0)  
参数同上

groups()和group同上

* sub
替换匹配的字符
> re.sub(pattern, repl, string, count=0)  
pattern : 正则中的模式字符串。  
repl : 替换的字符串，也可为一个函数。  
string : 要被查找替换的原始字符串。  
count : 模式匹配后替换的最大次数，默认 0 表示替换所有的匹配。  

eg：
```
#!/usr/bin/python3
import re

phone = "2004-959-559 # 这是一个电话号码"

# 删除注释
num = re.sub(r'#.*$', "", phone)
print ("电话号码 : ", num)

# 移除非数字的内容
num = re.sub(r'\D', "", phone)
print ("电话号码 : ", num)
```

repl是个函数  
eg：
```
#!/usr/bin/python

import re

# 将匹配的数字乘于 2
def double(matched):
    value = int(matched.group('value'))
    return str(value * 2)

s = 'A23G4HFD567'
print(re.sub('(?P<value>\d+)', double, s))
```

可替换标志符
![](./img/flags.png)
正则表达式
![](./img/python_regular_expression.png)
