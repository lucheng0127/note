###python爬虫推荐使用requests来加载网页，和BeautifulSoup来解析网页。
** urllib.request.Request **  
用于构建http请求。  
> urllib.request.Request(url, data=None, headers={}, origin_req_host=None, unverifiable=False, method=None)

* url：请求链接，必填。
* data：要传必须是bytes（字节流）类型，如果是字典可以先用urllib.parse.urlencode()编码。
* header：字典形式，可以在构造的时候添加传递参数，也可以通过Request.add_header()来添加请求头。*请求头主要用处：修改User-Agent来伪装浏览器，eg：（firefox：{‘User-Agent’: 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'}）。*
* origin_req_host：请求方的host或者IP。
* unverfiable：这个请求是否是无法验证的。
* method：GET，POST， PUT ...

```
#-*- coding: utf-8 -*-

from urllib import request, parse

url = "http://10.70.27.15/login/"

headers = {
    'User-Agent': 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)',
    'Host': '10.46.10.90',
}

data_params = {
    'email': 'wolf_sly@163.com',
    'password': '1234qwer'
}

data = bytes(parse.urlencode(data_params), encoding='utf-8')
req = request.Request(url=url, data=data, headers=headers, method='POST')
response = request.urlopen(req)
print(response.read().decode('utf-8'))
```
urlopen 发送请求
***
** urllib.request.BaseHandler **
* HTTPDefaultErrorHandler：用于处理HTTP响应错误，错误都会抛出HTTPError类型的异常。
* HTTPRedirectHandler：用于处理重定向。
* HTTPCookieProcessor：用于i处理Cookie。
* ProxyHandler：用于设置代理。
* HTTPPasswordMgr：用于管理密码，它维护用户名密码的表。
* HTTPBasicAuthHandler：用于管理认证。

其他参见python标准库官方文档  
基本用法eg：
```
#-*- coding: utf-8 -*-

from urllib import request

url = "http://10.70.27.15/login/"

auth_handler = request.HTTPBasicAuthHandler()
auth_handler.add_password(realm='test',
                          uri=url,
                          user='wolf_sly@163.com',
                          passwd='1234qwer')
opener = request.build_opener(auth_handler)
request.install_opener(opener)
request.urlopen(url)
```
***
代理访问：
```
#-*- coding: utf-8 -*-

from urllib import request

proxy_handler = request.ProxyHandler({
    'http': 'http://10.70.27.15:80'
})
opener = request.build_opener(proxy_handler)
response = opener.open('https://www.baidu.com')#通过http代理去访问baidu
print(response.read())
```
***
cookie
```
#-*- coding: utf-8 -*-

import http.cookiejar
import urllib.request

##保存cookie

url = 'https://www.baidu.com'
# filename = 'cookie.txt'
# cookie = http.cookiejar.LWPCookieJar(filename)
# handler = urllib.request.HTTPCookieProcessor(cookie)
# opener = urllib.request.build_opener(handler)
# response = opener.open(url)
# cookie.save(ignore_discard=True, ignore_expires=True)

##载入cookie
cookie = http.cookiejar.LWPCookieJar()
cookie.load('cookie.txt', ignore_discard=True, ignore_expires=True)
handler = urllib.request.HTTPCookieProcessor(cookie)
opener = urllib.request.build_opener(handler)
response = opener.open(url)
print(response.read().decode('utf-8'))
```
***
** urllib.error **
urllib.error 定义了urllib.request产生的异常。如果出现error，urllib.request会抛出
urllib.error。  
* urllib.error.URLError  

每一个urllib.request产生的异常都可以通过urllib.error来处理，它具有一个属性reason，返回错误
原因。  
```
#-*- coding: utf-8 -*-

import urllib.request
import urllib.error

try:
    response = urllib.request.urlopen('http://10.70.27.15/login')
except urllib.error.URLError as e:
    print(e.reason)
```

* urllib.error.HTTPError  

专门处理HTTP请求错误，三个属性：  
> * code：返回HTTP状态码
* reason：错误原因
* headers：HTTP响应头部

***
** urllib.parse **
解析链接  
```
#-*- coding: utf-8 -*-

from urllib.parse import urlparse

url = 'http://10.70.27.15/login'
result = urlparse(url)
print(result)
```
结果：
> ParseResult(scheme='http', netloc='10.70.27.15', path='/login', params='', query='', fragment='')

参数：
> urllib.parse.urlparse(urlstring, scheme='', allow_fragments=True)

scheme：协议类型（默认http和https）  
** urllib.parse.urlunparse() **  
和urlparse对立：  
```
#-*- coding: utf-8 -*-

from urllib.parse import urlunparse

data = ['http', 'www.baidu.com', 'index.html', 'user', 'a=6', 'comment']
print(urlunparse(data))
```
** urllib.parse.urlsplit() **  
和urlparse相似  
** urllib.parse.urlunsplit() **  
和urlunparse相似  
** urllib.parse.urljoin() **  
将字符串拼接成url  
***
** urllib.robotparser **  
robots:协议  
指定哪些网页可以抓取，哪些不可以抓取。  
文件名一般为robots.txt
```
User-agent: *
Disallow: /
Allow: /public/
```
和网站入口文件放一起例如index.html等。  
User-agent设置哪些爬虫可以爬取，\*代表所有都可以。
常见spider名称：
![](./img/spider_name.png)
** urllib.robotparser.RobotFileParser **  
解析robot.txt文件
```
from urllib.robotparser import RobotFileParser

rp = RobotFileParser()
rp.set_url('http://www.jianshu.com/robots.txt')
rp.read()
print(rp.can_fetch('*', 'http://www.jianshu.com/p/b67554025d7d'))
print(rp.can_fetch('*', "http://www.jianshu.com/search?q=python&page=1&type=collections"))
```
***
