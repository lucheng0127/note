[TCP/IP详解](http://blog.csdn.net/goodboy1881/article/category/204448)
![TCP/IP model](http://images.cnblogs.com/cnblogs_com/bluetzar/TCP4.jpg)
1. 数据链路层：（对应OSI的数据链路层和物理层）
2. 网络互联层：  
  IP协议就在这这一层，它负责对数据加上IP地址和其他的数据以确定传输目标（确定数据的接受目标正确）
3. 传输层：  
  在TCP/IP模型中，传输层的功能是使源端主机和目标主机上的对等实体可以进行会话。（TCP传输控制协议和UDP用户数据报协议）  
  TCP协议是一个面向连接的、可靠的协议。它将一台主机发出的字节流无差错地发往互联网上的其他主机。在发送端，它负责把上层传送下来的字节流分成报文段并传递给下层。在接收端，它负责把收到的报文进行重组后递交给上层。TCP协议还要处理端到端的流量控制，以避免缓慢接收的接收方没有足够的缓冲区接收发送方发送的大量数据。  
  UDP协议是一个不可靠的、无连接协议，主要适用于不需要对报文进行排序和流量控制的场合。
4. 应用层：（OSI中的会话层和表示层）
  应用层面向不同的网络应用引入了不同的应用层协议。其中，有基于TCP协议的，如文件传输协议（File Transfer Protocol，FTP）、虚拟终端协议（TELNET）、超文本链接协议（Hyper Text Transfer Protocol，HTTP），也有基于UDP协议的。


#### TCP/IP报文格式
TCP/IP报文封装  
![TCP/IP报文封装](http://images.cnblogs.com/cnblogs_com/bluetzar/IPPackage.jpg)
#### IP头部数据格式：
![](http://images.cnblogs.com/cnblogs_com/bluetzar/IPPackageHead.jpg)
* 版本字段：4bit，表明现在的IP版本。
* 报头长度：4bit
* 服务类型：8bit
* 总长度字段：16bit
* 标志字段：16bit， 用来唯一的标示主机发送到每一份数据报，通常发一份就加1
* 标志位字段： 3bit， 标志一份数据是否要分段
* 段偏移字段： 13bit
* 生存期（TTl： time to live）：8bit，用来设置数据报最多可以经过的路由数。由发送源主机设置，每经过一个路由减一，到0被丢弃。
* 协议字段：8bit， 指明上层的协议类型，如ICMP， TCP， UDP等
* 头部校验字段：16bit
* 源地址：32bit，发送端IP地址
* 目标地址：32bit，目标源IP地址

#### TCP数据段格式：
TCP是一种可靠的面向连接的字节流服务，源主机在传输数据前要先和目标主机建立连接，然后被编号的数据段按序发送。
![](http://images.cnblogs.com/cnblogs_com/bluetzar/TCPPackageHead.jpg)
* 源，目标端口：各16bit，TCP协议通过使用端口来标识源端口和目标端口的应用进程。端口号可以使用0到65535之间的任何数字，在收到服务请求时，操作系统动态地为客户端分配端口号。
* 顺序号：32bit，用来标识从TCP源端向TCP目标端发送的数据字节流，他表示在这个报文段中的第一个数据字节。
* 确认号：32bit， 当ACK标志为1时才有效，标示目标端期望收到源端的下一个数据字节。
* 标志物（U，A，P，R，S，F）：6bit含义如下：
  > URG 紧急指针有效  
  > ACK 确认序号有效  
  > PSH 接收方应尽快将该报文交给应用层  
  > RST 重新连接  
  > SYN 发起一个连接  
  > FIN 释放一个连接
* 窗口大小： 16bit， 本机期望一次接收的字节数
* TCP校验： 16bit， 对整个报文段进行校验
* 紧急指针： 16bit
* 选项： 32bit

#### UDP数据段格式
UDP是一种不可靠的、无连接的数据报服务。源主机在传送数据前不需要和目标主机建立连接。数据被冠以源、目标端口号等UDP报头字段后直接发往目的主机。这时，每个数据段的可靠性依靠上层协议来保证。在传送数据较少、较小的情况下，UDP比TCP更加高效。
![](http://images.cnblogs.com/cnblogs_com/bluetzar/UDPPackageHead.jpg)
* 源、目标端口号字段：占16比特。作用与TCP数据段中的端口号字段相同，用来标识源端和目标端的应用进程。
* 长度字段：占16比特。标明UDP头部和UDP数据的总长度字节。
* 校验和字段：占16比特。用来对UDP头部和UDP数据进行校验。和TCP不同的是，对UDP来说，此字段是可选项，而TCP数据段中的校验和字段是必须有的。

#### 套接字（socket）
每个TCP和UDP数据段都包含源，目标端口字段。把一个IP地址和一个端口号合称一个套接字，而一个套接字对（socket pair）可以唯一确定互联网络中每个TCP连接的双方。
![](http://images.cnblogs.com/cnblogs_com/bluetzar/Socket.jpg)
常见协议对应的端口
#### TCP连接建立与释放
![](http://images.cnblogs.com/cnblogs_com/bluetzar/TcpCon.jpg)
1. 源主机发送一个SYN=1的请求
2. 目标主机回一个SYN=1，ACK=1，同时在确认序号表明下一个数据段的序号。
3. 源主机回复ACK=1，连接建立。
4. TCP释放
