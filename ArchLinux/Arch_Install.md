1. 查看是否联网
```ping www.ustc.edu.cn #看是否有网（wifi-menu   or    dhcpcd）```
2. 分区
```cfdisk
mkfs.ext4 /dev/sda1
mkswap /dev/sda2
swapon /dev/sda2```
3. 挂载
```mount /dev/sda1 /mnt```
4. 更新mirrors源(先备份）
[参见科大](http://mirrors.ustc.edu.cn/)
5. 安装系统
```pacstrap /mnt base base-devel net-tools```
6. 生产fstab
```genfstab -U -p /mnt >>/mnt/etc/fstab```
7. 改变目录
```arch-chroot /mnt /bin/bash```
8. 配置locale
```vim /etc/locale.gen```,
```locale-gen```
9. ```vim vconsole.conf```

    > KEYMAP=us

    > FONT=

10. 时区
```ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime```
11. 主机名
```echo *** >> /etc/hostname```
12. 生成ramdisk
```mkinitcpio -p linux```
13. 用户配置
  > passwd(修改root密码）

  > useradd -m -g users -G wheel -s /bin/bash 自己用户名`

  > passwd 自己用户名

14. 安装grub
  > pacman -S grub-bios

  > grub-install /dev/sda

  > cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo/boot/grub/locale/en.mo

15. 网上说这一步解决了一个bug
  > grub-mkconfig -o /boot/grub/grub.cfg

16. 重启
  > exit

  > umount /mnt

  > reboot

17. 安装声卡
```pacman -S alsa-utils```

18. 显卡
```pacman -S mesa xf86-video-intel```

19. 触摸板
```pacman -S xf86-input-synaptics```

20. 安装xorg
```pacman -S xorg```

21. 安装WM和软件

### 触摸板配置
* 安装Synaptics驱动
```pacman -S xf86-input-synaptics```
* 管理工具
```yaourt -S gpointing-device-settings```
* 环境配置
```
#file: /etc/X11/xorg.conf.d/50-synaptics.conf
Section "InputClass"
        Identifier "touchpad catchall"
        Driver "synaptics"
        MatchIsTouchpad "on"

        Option "TapButton1" "1"            #单指敲击产生左键事件
        Option "TapButton2" "2"            #双指敲击产生中键事件
        Option "TapButton3" "3"            #三指敲击产生右键事件

        Option "VertEdgeScroll" "on"       #滚动操作：横向、纵向、环形
        Option "VertTwoFingerScroll" "on"
        Option "HorizEdgeScroll" "on"
        Option "HorizTwoFingerScroll" "on"
        Option "CircularScrolling" "on"  
        Option "CircScrollTrigger" "2"

        Option "EmulateTwoFingerMinZ" "40" #精确度
        Option "EmulateTwoFingerMinW" "8"
        Option "CoastingSpeed" "20"        #触发快速滚动的滚动速度

        Option "PalmDetect" "1"            #避免手掌触发触摸板
        Option "PalmMinWidth" "3"          #认定为手掌的最小宽度
        Option "PalmMinZ" "200"            #认定为手掌的最小压力值
EndSection
```
