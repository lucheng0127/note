LDAP(lightweight Directory Access Protocol)  
结构：  
![](./img/ldap_golbal_view.png)
** 查询过程： **
1. 客户端向LDAP Server1发起ldap请求
2. LDAP Server3返回信息告诉客户端去向下一层的服务器请求，假设数据在LDAP
Server1,则去返回下一层LDAP Server1的信息给客户端。
3. 客户端向LDAP Server1发送请求。
4. LDAP Server1 返回请求所需的信息给客户端。
