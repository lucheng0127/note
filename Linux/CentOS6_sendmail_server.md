#### 建议操作
1. 查看当前CentOS系统版本
> rpm --query centos-release

2. 更新USTC源
> [参见CentOS配置USTC源](http://mirrors.ustc.edu.cn/help/centos.html)

#### 正式配置sendmail服务
1. 安装sendmail package
> yum install sendmail sendmail-cf m4

2. 配置sendmail服务  
[配置参见_1](http://www.binarytides.com/linux-mail-command-examples/)
[配置参见_2](https://sachinsharm.wordpress.com/2013/08/19/setting-up-sendmail-on-centosrhel-6-3/)
配置文件说明
  * access 允许或者禁止某些系统调用该服务
  * domaintable 域名系统相关
  * local-host-names 定义host的aliases
  * mailertable 路由和代理相关
  * virtusertable 一台机器配置多个host

*修改配置后要使用如下命名使之生效*
> m4 sendmail.mc > sendmail.cf

无特殊设置保持默认配置即可默认通过25端口发送邮件。
> service sendmail restart

3. 修改发送邮件程序
Server为127.0.0.1
login注释
