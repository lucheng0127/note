#### shell 编程
eg:hello.sh
```
#!/bin/bash

echo "The first shell program."
```
> sudo chmod +x hello.sh #给脚本添加可执行权限  
./hello.sh #运行脚本

### shell 变量
变量定义不需要添加dollar符号，使用时需要。
eg：
```
your_name='something' #等号左右不能加空格
echo $your_name
echo ${your_name}
```

#### 只读变量
```
#!/bin/bash

my_name="wolf"
readonly my_name
my_name="lucheng" #尝试改变一个只读变量，会报错
```

#### 删除变量
> unset variable_name

#### 变量类型
* 局部变量：  
在脚本中定义，只在当前shell实例中有效，其他shell启动时不可以访问。
* 环境变量：  
所有程序，包括shell程序都可以访问环境变量。可以自定义环境变量。
* shell变量：  
shell变量是有shell程序设置的特殊变量。有一部分是环境变量有一部分是局部变量。

#### shell字符串
eg：
```
str_1='this is str 1'
str_2="this is str 2"
```
##### 单引号
* 单引号里面的任何字符都会原样输出，单引号里面的变量是无效的。
* 单引号中不能出现单引号（转义也不行）。

##### 双引号
* 双引号里面可以有变量。
* 可以转义（shell里面转移在前面加反斜杠"\"）

eg：
```
name='wolf'
echo "I'm \"$name\" !"
```

#### 字符串拼接
eg：
```
#!/bin/bash

str="The first shell script"
str_2='this is str 2'
name='lucheng'
greeting_1="hello,"$name"!"
greeting_2="hello,${name} !"

echo str 1 is $str
echo str 2 is $str_2
echo "I'm \"$name\"!"
echo $greeting_1 $greeting_2
```

#### 获取字符串长度
```
str='lucheng'
echo ${#str}
```

#### 提取字符串
```
str='I am lucheng'
echo ${str:0:3}
```

#### 查找字符串
```
#!/bin/bash

str="The first shell script"
str_2='this is str 2'
name='lucheng'
greeting_1="hello,"$name"!"
greeting_2="hello,${name} !"

echo str 1 is $str
echo str 2 is $str_2
echo "I'm \"$name\"!"
echo $greeting_1 $greeting_2
echo `expr index "$str" f` #这里是反引号“`”
```

#### shell 数组
支持一维数组但不支持多维数组。

##### 定义数组
```
array_name=(value0 value1 value2)

array_name_2=(
value0
value1
value2
)
```

##### 读取数组
> ${数组名[下标]}

用@获取所有元素
eg:
```
echo ${array_name[@]} #读取array_name里面的所有元素
```

#### shell 注释
eg：
```
#--------------------------------------------
# 这是一个注释
# author：菜鸟教程
# site：www.runoob.com
# slogan：学的不仅是技术，更是梦想！
#--------------------------------------------
##### 用户配置区 开始 #####
#
#
# 这里可以添加脚本描述信息
#
#
##### 用户配置区 结束  #####
```

### shell 传参
eg：
```
#!/bin/bash

echo "执行的文件为: $0"
echo "第一个参数为: $1"
echo "第二个参数为: $2"
```
执行：
> ./parameter.sh  
./parameter.sh 1  
./parameter.sh 1 2  

*其他*
>
$# 传递到脚本的参数个数  
$* 以一个单字符串显示所有传入参数。用"把$* 括起来将以"$1 $2"输出  
$$ 脚本运行的当前进程ID  
$! 后台运行的最后一个进程ID  
$@ 同$*  
$- 显示shell使用的当前选项  
$? 显示最后命令的退出状态，0表示没有错误，其他任何值都表示有错误  

#### shell 基本运算符
bash不支持数学运算，可以使用expr来实现。  
eg：
```
#!/bin/bash

val=`expr 2 + 2` #赋值等号两端不加空格，运算符两端要空格，expr外面的是反引号"`"同markdown里面的code引用的那个
echo "两个数之和为: $val"
```
shell_算术运算符
![shell_算术运算符.png](./img/shell_算术运算符.png)
shell_关系运算符
![shell_关系运算符.png](./img/shell_关系运算符.png)
shell_布尔运算符
![shell_布尔运算符.png](./img/shell_布尔运算符.png)
shell_逻辑运算符
![shell_逻辑运算符.png](./img/shell_逻辑运算符.png)
shell_字符串运算符
![shell_字符串运算符.png](./img/shell_字符串运算符.png)
shell_文件测试运算符
![shell_文件测试运算符.png](./img/shell_文件测试运算符.png)

#### echo
##### 转义
echo -e 开启转义
eg：
```
echo -e "OK! \n" # -e 开启转义
echo "It it a test"

echo -e "OK! \c" # -e 开启转义 \c 不换行
echo "It another test"
```

##### 结果定向至文件
> echo 'hello' > hello.txt

##### 原样输出字符，不转义
> echo '$name\'

##### 显示命令执行结果
> echo `date`
显示当前日期

#### shell printf 命令
eg:
```
printf "%-10s %-8s %-4s\n" 姓名 性别 体重kg  
printf "%-10s %-8s %-4.2f\n" 郭靖 男 66.1234
printf "%-10s %-8s %-4.2f\n" 杨过 男 48.6543
printf "%-10s %-8s %-4.2f\n" 郭芙 女 47.9876
```

shell_printf转义
![shell_printf转义.png](./img/shell_printf转义.png)

#### shell test
检测条件是否成立
eg:
```
num1=100
num2=100
if test $[num1] -eq $[num2]
then
    echo '两个数相等！'
else
    echo '两个数不相等！'
fi
```

#### shell 流程控制
##### if
```
if condition
then
    command1
    command2
    ...
    commandN
fi
```

##### if else
```
if condition
then
    command1
    command2
    ...
    commandN
else
    command
fi
```

##### if else-if else
```
if condition1
then
    command1
elif condition2
then
    command2
else
    commandN
fi
```

##### for
```
for var in item1 item2 ... itemN
do
    command1
    command2
    ...
    commandN
done
```
或者
```
for var in item1 item2 ... itemN; do command1; command2… done;
```

##### while
```
while condition
do
    command
done
```

##### 无限循环
```
while :
do
    command
done
```

##### until 循环
until执行一系列命令直到条件为真停止
```
until condition
do
    command
done
```

##### case
```
case 值 in
模式1)
    command1
    command2
    ...
    commandN
    ;;
模式2）
    command1
    command2
    ...
    commandN
    ;;
esac
```
例如
```
echo '输入 1 到 4 之间的数字:'
echo '你输入的数字为:'
read aNum
case $aNum in
    1)  echo '你选择了 1'
    ;;
    2)  echo '你选择了 2'
    ;;
    3)  echo '你选择了 3'
    ;;
    4)  echo '你选择了 4'
    ;;
    *)  echo '你没有输入 1 到 4 之间的数字'
    ;;
esac
```

##### 跳出循环
>
break  
continue  
esac #用于case中

#### shell 函数
```
#!/bin/bash

demoFun(){
    echo "这是我的第一个 shell 函数!"
}
echo "-----函数开始执行-----"
demoFun
echo "-----函数执行完毕-----"
输出结果：
-----函数开始执行-----
这是我的第一个 shell 函数!
-----函数执行完毕-----
下面定义一个带有return语句的函数：
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com

funWithReturn(){
    echo "这个函数会对输入的两个数字进行相加运算..."
    echo "输入第一个数字: "
    read aNum
    echo "输入第二个数字: "
    read anotherNum
    echo "两个数字分别为 $aNum 和 $anotherNum !"
    return $(($aNum+$anotherNum))
}
funWithReturn
echo "输入的两个数字之和为 $? !"
```
```
#!/bin/bash

funWithParam(){
    echo "第一个参数为 $1 !"
    echo "第二个参数为 $2 !"
    echo "第十个参数为 $10 !"
    echo "第十个参数为 ${10} !"
    echo "第十一个参数为 ${11} !"
    echo "参数总数有 $# 个!"
    echo "作为一个字符串输出所有参数 $* !"
}
funWithParam 1 2 3 4 5 6 7 8 9 34 73
```

#### shell 输出/输入重定向
![shell_重定向.png](./img/shell_重定向.png)

##### 输出到null
> command > /dev/null

#### shell 文件包含
引用外部脚本文件
```
. filename   # 注意点号(.)和文件名中间有一空格

或

source filename
```
