## 关系型数据库设计

### 范式：
[范式概念及例子详解](http://www.cnblogs.com/CareySon/archive/2010/02/16/1668803.html)
##### 范式目标：
* 减少数据冗余
* 消除异常

#### 缺点：
* 增加了查询的复杂度
* 影响数据库查询性能

### ER图标记方法:
![标记方法](http://images.cnblogs.com/cnblogs_com/DBFocus/201104/201104241146088597.png)

### 数据库设计教程：
> 参见：
[数据库设计step by step](http://blog.csdn.net/luoweifu/article/details/8871289)

> 参见：
[database design tutorial](http://en.tekstenuitleg.net/articles/software/database-design-tutorial/intro.html)
