#### MITM(man-in-the-middle attack)中间人攻击
[中间人攻击wiki](https://zh.wikipedia.org/wiki/%E4%B8%AD%E9%97%B4%E4%BA%BA%E6%94%BB%E5%87%BB)
***
** ARP协议基础 **  
ARP是用于将网路层的IP解析成数据链路层的MAC地址的。  
* ARP协议格式  
![](img/ARP_data_type.png)
当源主机知道目标主机的IP地址而不知道对方的MAC地址的时候可以使用该协议获取对方MAC地址，主机通信是通过MAC地址来实现的。  

```
1 以太网首部
  1.1 以太网目的地址：6字节的硬件地址地址。
    1) ARP请求解析包：因为客户端这个时候并不知道目标IP的MAC地址，所以“ARP请求解析包”一定是一个数据链路层广播数据包，这个字段一定是FF:FF:FF:FF:FF:FF（广播MAC地址）
    2) ARP解析回应包：硬件地址，包含发送方的MAC地址，当广播发出时，目标主机的网络协议栈收到这个数据包，发现是一个ARP解析包，并且target IP是自己，就对这个数据包的发送方进行回应。
  1.2 以太网源地址： 6字节硬件地址，发送ARP包的主机地址。
  1.3 以太帧类型：用来表明上层协议的类型，在ARP协议中该字段值为0806.
2 ARP数据包
  2.1 ARP数据包头部
    2.1.1 硬件类型：指明了发送方想知道的硬件接口类型，以太网的值为1
    2.1.2 协议类型: 指明了发送方提供的高层协议类型，即哪个协议想要使用ARP解析服务，IP为0800(16进制)，一般情况下都是IP协议在使用ARP服务
    2.1.3 硬件地址长度: 指的是MAC地址的长度，长度为6 单位是字节
    2.1.4 协议长度: 如果是IP4则这个值为4,单位是字节
    2.1.5 操作类型(op): 用来表示这个报文的类型
      1） ARP请求为1
      2） ARP响应为2
      3） RARP请求为3
      4） RARP响应为4
  2.2 ARP数据字段
    2.2.1 发送方硬件地址: 发送方的MAC地址
    2.2.2 发送方IP地址: 发送方的IP地址
    2.2.3 目标硬件地址
      1） ARP请求解析包： 00:00:00:00:00:00，因为请求方这个时候并不知道目标主机的MAC地址，所以填全0
      2） ARP解析回应包： 请求方的MAC地址，这个目标主机返回给请求方的解析回应包
    2.2.4 目标IP地址: 目的主机的IP地址
```
* ARP 运行机制
![](img/ARP_work_line.png)
* 保证ARP正常工作的策略

```
1. 设置静态的MAC-->IP对应表，不要让本机刷新设定好的转换表，这是一个很有效的方法
    1) 网关
    将所有的客户端的MAC以及对应IP进行静态绑定，防止攻击者假冒客户端
    2) 普通客户端
    绑定网关MAC地址，防止攻击者假冒网关
2. 停止使用ARP，使用静态ARP缓存表，将ARP做为永久条目保存在对应表中。
3. 管理员定期轮询当前网络、主机健康情况，检查主机上的ARP缓存。  
```
***
** 常见攻击场景 **
##### 有线局域网下ARP投毒+中间人伪造SSL证书攻击
** ARP投毒 **  
1. 主机C冒充网关欺骗主机B
主机C向主机B发送伪造的ARP回应包，声称自己的MAC地址就是网关对应的IP，这样，主机B就会将所有的流量发送主机C(攻击者)，主机C开启ip_forward路由转发功能将数据包进行转发
![](img/ARP_trick_1.png)

```
1. 开启端口转发，允许本机像路由器那样转发数据包
echo 1 > /proc/sys/net/ipv4/ip_forward
2. ARP投毒，向主机B声称自己(攻击者)就是网关
arpspoof -h
Usage: arpspoof [-i interface] [-t target] host
arpspoof -i eth0 -t 192.168.159.132 192.168.159.2
(192.168.159.132是我们的攻击目标、192.168.159.2是网关IP地址)
```

##### 有线局域网下ARP投毒+中间人SSL卸载攻击

##### 基于中间人攻击的SSL BEAST攻击

##### 基于DNS劫持的SET社会工程钓鱼攻击

##### 基于中间人攻击的会话劫持攻击（Session Hajacking）
