### git push 一般步骤
1. 添加密钥
2. 把本地目录与远程仓库关联
```
git remote add origin git@github.com:***/***
```
3. 添加文件进缓存 git add .
4. 提交修改 git commit -m '修改描述'
5. 上传某分支 eg:
```
git add .
git commit -m 'first_commit'
git remote add origin git@github.com:***/***
git push origin master
```

### FAQ:
> fatal: remote origin already exists

```
git remote rm origin
```
> error:failed to push som refs to.......

```
git pull origin master
```
> fatal: refusing to merge unrelated histories

```
git merge origin/master --allow-unrelated-histories
```
### 团队合作
[团队合作参见](http://mp.weixin.qq.com/s?__biz=MzA4NTQwNDcyMA==&mid=2650661978&idx=1&sn=2f5329f5b2bfda7050822cc5e3a4f03f&mpshare=1&scene=1&srcid=0117iSnGcNQpOshg5l1HCFlc#rd)
[进阶参见](http://mp.weixin.qq.com/s?__biz=MzA4NTQwNDcyMA==&mid=2650661929&idx=1&sn=69e00516a30723c5a20af3c7a84173a4&mpshare=1&scene=1&srcid=0117JTmcX95eWJXsO72Ng1HR#rd)
